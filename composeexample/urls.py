from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<str:id>/execute/', views.execute, name='execute'),
]