from django.http import HttpResponse
from django.template import loader

#modules
import pandas as pd 
from pandas.io.json import json_normalize
import requests

def index(request):
	form="""<form action=01/execute method="get">  	
			<label for="month">Mes:</label><br><input type="motnh" id="month" name="month"><br>
			<label for="year">Year:</label><br><input type="year" id="year" name="year">
			<br>
			<input type="radio" id="male" name="option" value="getXLS">
			<label for="male">CSV</label><br>
			<input type="radio" id="female" name="option" value="show">
			<label for="female">show</label><br>
			<input type="submit" value="Submit">
			</form>"""
	return HttpResponse(form)

def execute(request,id):
	if request.method == 'GET':
		month=request.GET['month']
		year=request.GET['year']
		option=request.GET['option']
		if option=='getXLS':
			jsonData=getJSON(year,month)
			data=json_normalize(jsonData,'UFs')
			response = HttpResponse(content_type='application/vnd.ms-excel')
			response['Content-Disposition'] = 'attachment; filename=UF'+year+'-'+month+'.csv'
			response.write(data.to_csv(sep=';'))
			return response
		if option=='show':
			jsonData=getJSON(year,month)
			data=json_normalize(jsonData,'UFs')
			return HttpResponse(data.to_html())
			
		return HttpResponse('WRONG PARAMETERS')
	
	

	
def getJSON(year,month):
	baseURL='https://api.sbif.cl/api-sbifv3/recursos_api/uf/'
	apikeyURL='/?&apikey=d8093171162117c0c6e8da895b00978d4e2b6a0e&formato=JSON'
	urlRequest = baseURL+year+'/'+month+apikeyURL
	r = requests.get(url=urlRequest)
	return r.json()
		
